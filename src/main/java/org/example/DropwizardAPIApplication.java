package org.example;

import io.dropwizard.Application;
import io.dropwizard.setup.Bootstrap;
import io.dropwizard.setup.Environment;
import org.example.resources.HelloWorldResource;
import org.example.resources.FibonacciResource;
import org.example.health.TemplateHealthCheck;

public class DropwizardAPIApplication extends Application<DropwizardAPIConfiguration> {

    public static void main(final String[] args) throws Exception {
        new DropwizardAPIApplication().run(args);
    }

    @Override
    public String getName() {
        return "DropwizardAPI";
    }

    @Override
    public void initialize(final Bootstrap<DropwizardAPIConfiguration> bootstrap) {
        // TODO: application initialization
    }

    @Override
    public void run(final DropwizardAPIConfiguration configuration,
                    final Environment environment) {
        final HelloWorldResource resource = new HelloWorldResource(
            configuration.getTemplate(),
            configuration.getDefaultName()
        );
        final FibonacciResource fibonacciResource = new FibonacciResource(
            "1"
        );
        final TemplateHealthCheck healthCheck = new TemplateHealthCheck(
            configuration.getTemplate()
        );
        environment.healthChecks().register("template", healthCheck);
        environment.jersey().register(resource);
        environment.jersey().register(fibonacciResource);
    }
}
