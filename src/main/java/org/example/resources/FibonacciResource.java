package org.example.resources;

import org.example.api.Fibonacci;
import com.codahale.metrics.annotation.Timed;

import javax.ws.rs.GET;
import javax.ws.rs.Path;
import javax.ws.rs.Produces;
import javax.ws.rs.QueryParam;
import javax.ws.rs.core.MediaType;
import javax.ws.rs.WebApplicationException;
import java.util.Optional;

@Path("/fibonacci")
@Produces(MediaType.APPLICATION_JSON)
public class FibonacciResource {
    private String defaultCount;

    public FibonacciResource(String defaultCount) {
        this.defaultCount = defaultCount;
    }

    @GET
    @Timed
    public Fibonacci calcFibonacci(@QueryParam("count") Optional<String> count) {
        final int value = Integer.parseInt(count.orElse(defaultCount));
        if (value < 1 || value > 100) {
            throw new WebApplicationException(400);
        }
        return new Fibonacci(value);
    }
}