package org.example.api;

import com.fasterxml.jackson.annotation.JsonProperty;
import java.lang.Math;
import java.math.BigInteger;

public class Fibonacci {
    private int member_count;
    private BigInteger[] sequence;
    private BigInteger total;
    private boolean isNegative;

    public Fibonacci() {
      // Jackson deserialization
    }

    public Fibonacci(int member_count) {
      if (member_count < 0) {
        this.member_count = Math.abs(member_count);
        this.isNegative = true;
      }
      else {
        this.member_count = member_count;
        this.isNegative = false;
      }
      FibonacciSequence();
      FibonacciTotal();
    }

    @JsonProperty
    public int getMemberCount() {
      return member_count;
    }

    @JsonProperty
    public BigInteger[] getSequence() {
      return sequence;
    }

    @JsonProperty
    public BigInteger getTotal() {
      return total;
    }

    private void FibonacciSequence() {
      if (this.member_count == 0) {
        this.sequence = new BigInteger[] {BigInteger.valueOf(0)};
        return;
      }
      if (this.member_count == 1) {
        if (this.isNegative) {
          this.sequence = new BigInteger[] {BigInteger.valueOf(-1)};
          return;
        }
        else {
          this.sequence = new BigInteger[] {BigInteger.valueOf(1)};
          return;
        }
      }
      BigInteger[] sequence_list = new BigInteger[this.member_count];
      if (this.isNegative) {
        sequence_list[0] = BigInteger.valueOf(-1);
        sequence_list[1] = BigInteger.valueOf(-2);
      }
      else {
        sequence_list[0] = BigInteger.valueOf(1);
        sequence_list[1] = BigInteger.valueOf(2);
      }

      for(int i = 2; i < this.member_count; i++) {
        sequence_list[i] = sequence_list[i-1].add(sequence_list[i-2]);
      }

      this.sequence = sequence_list;
    }

    private void FibonacciTotal() {
      BigInteger total = BigInteger.valueOf(0);
      for(int i = 0; i < this.member_count; i++) {
        total = total.add(this.sequence[i]);
      }

      this.total = total;
    }
}