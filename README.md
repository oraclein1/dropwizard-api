# DropwizardAPI

How to start the DropwizardAPI application
---

1. Run `mvn clean install` to build your application
1. Start application with `java -jar target/dropwizard-api-1.0-SNAPSHOT.jar server config.yml`
1. To check that your application is running enter url `http://localhost:8080`

Health Check
---

To see your applications health enter url `http://localhost:8081/healthcheck`

How to build and run DropwizardAPI Docker container
---

1. Run `docker build -t oracle-app-server:latest -f DOCKERFILE .`. It is recommended to specify your own tag using the -t option.
2. Run `docker run -p 8080:8080 oracle-app-server:latest`. If you specified a different tag, then use that instead.